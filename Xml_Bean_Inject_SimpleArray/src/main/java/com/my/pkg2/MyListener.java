package com.my.pkg2;

import java.util.Arrays;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@WebListener
public class MyListener implements ServletContextListener {

	public void contextInitialized(ServletContextEvent sce) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("myBeanDefinitions.xml");
		
		Student student = (Student) context.getBean("student");
		
		System.out.println(Arrays.toString(student.girlFriend));
		System.out.println("***************************");
		System.out.println(student.age);
	}	
	
}
