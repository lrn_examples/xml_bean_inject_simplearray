<html>
<body>

	ApplicationContext injects bean (Student) setting up its array fields

<pre>	
	&lt;bean id="student" class="com.my.pkg2.Student">
		&lt;property name="girlFriend">
			&lt;list>
				&lt;value>"Mary"&lt;/value>
				&lt;value>"Jane"&lt;/value>
				&lt;value>"Jessica"&lt;/value>
			&lt;/list>
		&lt;/property>

		&lt;property name="age" value="23">&lt;/property>
</pre>
	

	<pre>
<b>src/main/java + (src/test/java + src/test/resources) shall be created manually</b>
(maven-archetype-webapp don't include them!). Just create simple folder
(Eclipse would automatically make it source folder).
If src/test/resources isn't made source folder do it yourself.

<b>java files compile only in src/main/java, </b> 
don't put them into src/main/resources !!! 
</pre>
</body>
</html>
